# wrouter
wrouter是最纯粹的s路由注册/解析器，它在不刷新页面的情况下，通过记录history或“#”组织不同的url路径，并通过URL执行不同回调方法。当使用history记录执行wrouter时可结合后台代码即有利于SEO，又有利于用户体验。
本框架需要jquery1.10以上版本jquery支持，兼容ie8及以上ie版本和其他主流浏览器，在不支持的浏览器下自动转换跳转url。
一、使用
    1.JS库引用
    <script type="text/javascript" src="js/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="js/wrouter-0.12.min.js"></script>
    本框架需引用jquery库的支持
    2.调用wrouter
```
<script type="text/javascript">
        var router = wrouter({
            html5history: false,
            onprogress: function (e) {
                $(router.progress).css("width", 0).show().animate({ "width": e.loaded / e.total * 100 + "%" }, function () {
                    if (e.loaded == e.total) $(router.progress).fadeOut(500);
                });
            },
            root: "/",
            routers: [{
                title: "aaaa",
                rule: "\\w+.html",
                container: "#container",
                progress: "#progress",
                action: function (result) {
                    var $result = $(result);
                    var $container = $("#container", $result);
                    if ($container && $container.length > 0)
                        return $container.html();
                    return result;
                }
            }]
        });
    </script>
```
    wrouter接受的参数是一个对象：
        对象属性解释：
            html5history： 布尔值 可缺省 不指定此参数系统判断浏览器是否支持pushstate，不支持则用#号处理路由
            onprogress：通过路由请求过程中的进度处理，如进度条。此处示例是进度条的效果
            root：设置当前路由设置的根文件
            routers：路由规则配置 对象数组集合
                routers的元素属性解释
                    title：文档标题，用于改变浏览器上的标题
                    rule：路由规则（字符串或有正则表达式的字符串或正则表达式对象）
                    container：路由请求后的内容容器
                    action：路由的请求后的回调方法
                    progress：进度条效果

配置ok，点击页面a标签后就能看到效果
![输入图片说明](http://git.oschina.net/uploads/images/2016/1117/111527_1eabe5e3_573552.png "在这里输入图片标题")